package streamdeck

const (
	//ActionTurnOnDevice is the UUID to turn on the device
	ActionTurnOnDevice = "dev.sean.lifx.on"
	//ActionTurnOffDevice is the UUID to turn on the device
	ActionTurnOffDevice = "dev.sean.lifx.off"
	//ActionToggleDevice is the UUID to toggle the power state of the device
	ActionToggleDevice = "dev.sean.lifx.toggle"
	//ActionSetColor is the UUID to set the color on the device
	ActionSetColor = "dev.sean.lifx.color"
	//ActionSetBrightness is the UUID to set the brightness on the device
	ActionSetBrightness = "dev.sean.lifx.brightness"
	//ActionSetWaveform is the UUID to set a waveform on the device
	ActionSetWaveform = "dev.sean.lifx.waveform"
	//ActionDebug is the UUID to generate a debug bundle
	ActionDebug = "dev.sean.lifx.debug"
)
