module gitlab.com/wwsean08/lifx-streamdeck

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/mostlygeek/arp v0.0.0-20170424181311-541a2129847a
	github.com/reugn/go-quartz v0.3.4
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/stretchr/testify v1.7.0
	gitlab.com/wwsean08/golifx v0.4.0
	gitlab.com/wwsean08/streamdeck v0.0.6
)
